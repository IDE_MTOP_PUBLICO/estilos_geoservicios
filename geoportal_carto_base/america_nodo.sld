<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>America del sur</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        
        <!--Regla para mostrar Uruguay con verde-->
        <Rule>
          <Name>Uruguay</Name>
          <ogc:Filter>
              <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>cntry_name</ogc:PropertyName>
              <ogc:Literal>Uruguay</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>120000</MinScaleDenominator>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill" >
                <ogc:Literal>#CFDBC4</ogc:Literal> <!--verde D7DFCF-->
              </CssParameter>
              <CssParameter name="fill-opacity" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#888888</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        
        <!--Regla para mostrar Uruguay de cerca-->
        <Rule>
          <Name>Uruguay</Name>
          <ogc:Filter>
             <ogc:PropertyIsEqualTo>
               <ogc:PropertyName>cntry_name</ogc:PropertyName>
               <ogc:Literal>Uruguay</ogc:Literal>
             </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>120000</MaxScaleDenominator>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill" >
                <ogc:Literal>#E0DAD2</ogc:Literal> 
              </CssParameter>
              <CssParameter name="fill-opacity" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#888888</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
        
        <!--Regla para mostrar el resto de América del sur-->
        <Rule> 
          <Name>America del Sur</Name>
          <ogc:Filter>
            <ogc:PropertyIsNotEqualTo>
            <ogc:PropertyName>cntry_name</ogc:PropertyName>
            <ogc:Literal>Uruguay</ogc:Literal>
            </ogc:PropertyIsNotEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill" >
                <ogc:Literal>#E0DAD2</ogc:Literal> 
              </CssParameter>
              <CssParameter name="fill-opacity" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke">#888888</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
            </Stroke>
          </PolygonSymbolizer>
          <TextSymbolizer>
            <Label>
              <ogc:PropertyName>pais</ogc:PropertyName>
            </Label>
            <Font>
              <CssParameter name="font-family">Arial</CssParameter>
              <CssParameter name="font-weight">normal</CssParameter>
              <CssParameter name="font-size">12</CssParameter>
            </Font>
            <LabelPlacement>
              <PointPlacement>
                <AnchorPoint>
                <AnchorPointX>0.5</AnchorPointX>
                <AnchorPointY>0.5</AnchorPointY>
              </AnchorPoint>
              </PointPlacement>
            </LabelPlacement>
            <Fill>
              <CssParameter name="fill">#2e2e2e</CssParameter>
            </Fill>
            <VendorOption name="autoWrap">100</VendorOption>
            <VendorOption name="maxDisplacement">200</VendorOption>
          </TextSymbolizer>
        </Rule>
        
        
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>