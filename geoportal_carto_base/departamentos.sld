<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Departamento</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        <Rule>
          <PolygonSymbolizer>
          <Stroke>
                        <CssParameter name="stroke">##585858</CssParameter>
                        <CssParameter name="stroke-width">1</CssParameter>
                      </Stroke>
          </PolygonSymbolizer>
          </Rule>
          <Rule>
          <MinScaleDenominator>20000</MinScaleDenominator>
          <TextSymbolizer>
              <Label>
                  <ogc:PropertyName>nombre</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">SansSerif</CssParameter>
                  <CssParameter name="font-weight">bold</CssParameter>
                  <CssParameter name="font-size">9</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.5</AnchorPointY>
                   </AnchorPoint>
                 </PointPlacement>
              </LabelPlacement>
              <Fill>
                  <CssParameter name="fill">#A4A4A4</CssParameter>
              </Fill>
          </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>