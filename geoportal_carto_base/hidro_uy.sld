<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Hidrograf�a</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
       <Rule>
         
          <!--Name>Cursos Navegables y flotables</Name-->
          <ogc:Filter>
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>nav_flot</ogc:PropertyName>
              <ogc:Literal>Si</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MinScaleDenominator>250000</MinScaleDenominator>
         <MaxScaleDenominator>25000000</MaxScaleDenominator>
          
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#2E9AFE</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
          <!--Name>Cursos No Navegables y flotables</Name-->
          <MinScaleDenominator>100</MinScaleDenominator>
          <MaxScaleDenominator>250000</MaxScaleDenominator>
          <LineSymbolizer>
            <Stroke>
              <CssParameter name="stroke">#2E9AFE</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>