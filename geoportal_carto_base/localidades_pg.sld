<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>localidades_pg</Name>
    <UserStyle>
      <Title>Localidad</Title>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        <Rule>
           <Name>localidad</Name>
           <MaxScaleDenominator>100000</MaxScaleDenominator>
           <PolygonSymbolizer>
              <Fill>
                <CssParameter name="fill" >
                  <ogc:Literal>#EBC398</ogc:Literal><!--FE9A2E-->
                </CssParameter>
                <CssParameter name="fill-opacity" >
                  <ogc:Literal>0.5</ogc:Literal>
                </CssParameter>
              </Fill>
              <Stroke>
                <CssParameter name="stroke" >
                  <ogc:Literal>#EBC398</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-opacity" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-width" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linecap" >
                  <ogc:Literal>butt</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linejoin" >
                  <ogc:Literal>bevel</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-dashoffset" >
                  <ogc:Literal>0.0</ogc:Literal>
                </CssParameter>
              </Stroke>
            </PolygonSymbolizer>
            <TextSymbolizer>
              
              <Label>
                  <ogc:PropertyName>nom_loc</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">Arial</CssParameter>
                  <CssParameter name="font-weight">normal</CssParameter>
                  <CssParameter name="font-size">12</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.5</AnchorPointY>
                   </AnchorPoint>
                 </PointPlacement>
              </LabelPlacement>
              <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
              </Fill>
          </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>