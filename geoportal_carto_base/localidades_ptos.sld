<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>localidades_ptos</Name>
    <UserStyle>
      <Title>Localidad</Title>
      <FeatureTypeStyle>
        <Rule>
          <Name>localidad_pto_2</Name>
          <Title>Localidad</Title>
          <MinScaleDenominator>100000</MinScaleDenominator>
          <MaxScaleDenominator>200000</MaxScaleDenominator>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#EBC398</CssParameter>
                </Fill>
                <Stroke>
                  <CssParameter name="stroke">#B06000</CssParameter>
                </Stroke>
              </Mark>
              <Size>8</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Name>localidad_pto_1</Name>
          <Title>Localidad</Title>
          <MinScaleDenominator>200000</MinScaleDenominator>
          <MaxScaleDenominator>1500000</MaxScaleDenominator>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#EBC398</CssParameter>
                </Fill>
                <Stroke>
                  <CssParameter name="stroke">#B06000</CssParameter>
                </Stroke>
              </Mark>
              <Size>4</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule> 
          <!-- <Name>localidad_pto_text</Name> -->
          <Title>Localidad</Title>
          <MinScaleDenominator>100000</MinScaleDenominator>
          <MaxScaleDenominator>350000</MaxScaleDenominator>
          <TextSymbolizer>
              <Label>
                  <ogc:PropertyName>nom_loc</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">Arial</CssParameter>
                  <CssParameter name="font-weight">bold</CssParameter>
                  <CssParameter name="font-size">12</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.0</AnchorPointY>
                   </AnchorPoint>
                   <Displacement>
                     <DisplacementX>0</DisplacementX>
                     <DisplacementY>5</DisplacementY>
                   </Displacement>
                 </PointPlacement>
              </LabelPlacement>
              <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
              </Fill>
          </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>