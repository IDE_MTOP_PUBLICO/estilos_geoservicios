<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>cuadricula</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        <Rule>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PolygonSymbolizer>
              <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#717171</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-dasharray">5 2</CssParameter>
                  <CssParameter name="stroke-width">
                      <ogc:Literal>1</ogc:Literal>
                  </CssParameter>
              </Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <MaxScaleDenominator>30000</MaxScaleDenominator>
          <TextSymbolizer>
            <Label>
                <ogc:PropertyName>name</ogc:PropertyName>
            </Label>
            <Font>
                <CssParameter name="font-family">SansSerif</CssParameter>
                <CssParameter name="font-weight">bold</CssParameter>
                <CssParameter name="font-size">12</CssParameter>
            </Font>
            <LabelPlacement>
               <PointPlacement>
                 <AnchorPoint>
                   <AnchorPointX>0.5</AnchorPointX>
                   <AnchorPointY>0.5</AnchorPointY>
                 </AnchorPoint>
               </PointPlacement>
            </LabelPlacement>
            <Fill>
                <CssParameter name="fill">#717171</CssParameter>
            </Fill>
          </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>