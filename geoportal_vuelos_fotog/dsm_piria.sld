<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>gtopo</Name>
    <UserStyle>
      <Name>dem</Name>
      <Title>DSM</Title>
      <Abstract>Classic elevation color progression</Abstract>
      <FeatureTypeStyle>
        <Rule>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap>
              <ColorMapEntry color="#000000" quantity="-500" label="nodata" opacity="0.0" />
              <ColorMapEntry color="#0000ff" quantity="1" label="1" />
              <ColorMapEntry color="#01A001" quantity="5" label="5" />
              <ColorMapEntry color="#00FF00" quantity="10" label="10" />
              <ColorMapEntry color="#FFFF00" quantity="15" label="15" />
              <ColorMapEntry color="#FF7F00" quantity="25" label="25" />
              <ColorMapEntry color="#FF6200" quantity="35" label="35" />
              <ColorMapEntry color="#FF3C00" quantity="45" label="45" />
              <ColorMapEntry color="#FF0000" quantity="55" label="55" />
              <ColorMapEntry color="#631700" quantity="100" label="100" />
              <ColorMapEntry color="#551400" quantity="150" label="150" />
              <ColorMapEntry color="#370D00" quantity="200" label="200" />
              <ColorMapEntry color="#000000" quantity="280" label="280" />
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>