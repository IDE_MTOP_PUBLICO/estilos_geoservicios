<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Espejos_de_Agua</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        <Rule>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill" >
                <ogc:Literal>#70c9ff</ogc:Literal>
              </CssParameter>
              <CssParameter name="fill-opacity" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
            </Fill>
            <Stroke>
              <CssParameter name="stroke" >
                <ogc:Literal>#2E9AFE</ogc:Literal>
              </CssParameter>
              <CssParameter name="stroke-opacity" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
              <CssParameter name="stroke-width" >
                <ogc:Literal>1.0</ogc:Literal>
              </CssParameter>
              <CssParameter name="stroke-linecap" >
                <ogc:Literal>butt</ogc:Literal>
              </CssParameter>
              <CssParameter name="stroke-linejoin" >
                <ogc:Literal>bevel</ogc:Literal>
              </CssParameter>
              <CssParameter name="stroke-dashoffset" >
                <ogc:Literal>0.0</ogc:Literal>
              </CssParameter>
            </Stroke>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>