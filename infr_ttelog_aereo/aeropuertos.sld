<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>Aeropuertos</Name>
    <UserStyle>
      <Name>aeropuertos</Name>
      <Title>Aerouertos</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>Aeropuerto Publico</Title>
          <ogc:Filter>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>dominio</ogc:PropertyName>
                <ogc:Literal>Publico</ogc:Literal>
              </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="aero_pub_n.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>
              <Size>35</Size>
             </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>Aeropuerto Privado</Title>
          <ogc:Filter>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>dominio</ogc:PropertyName>
                <ogc:Literal>Privado</ogc:Literal>
              </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>2000000</MaxScaleDenominator>
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="aero_priv_n3.svg" />
                <Format>image/svg+xml</Format>
              </ExternalGraphic>
              <Size>25</Size>
             </Graphic>
          </PointSymbolizer>
        </Rule>
            </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>