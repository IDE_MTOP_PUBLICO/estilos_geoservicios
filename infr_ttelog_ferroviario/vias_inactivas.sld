<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>via ferrea inactiva</Name>
    <UserStyle>
      <Name>via ferrea inactiva</Name>
      <Title>Vias ferreas inactivas</Title>
      <Abstract></Abstract>

      <FeatureTypeStyle>
         <Rule>
           <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
             <Stroke>
               <CssParameter name="stroke">#6E6E6E</CssParameter>
               <CssParameter name="stroke-width">1</CssParameter>
               
             </Stroke>
           </LineSymbolizer>
           <LineSymbolizer>
               <Stroke>
                   <GraphicStroke>
                       <Graphic>
                           <Mark>
                             <WellKnownName>shape://vertline</WellKnownName>
                             <Stroke>
                                <CssParameter name="stroke">#6E6E6E</CssParameter>
                                <CssParameter name="stroke-width">1</CssParameter>
                             </Stroke>
                           </Mark>
                           <Size>8</Size>
                       </Graphic>
                   </GraphicStroke>
                 </Stroke>
           </LineSymbolizer>
         </Rule>
       </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>