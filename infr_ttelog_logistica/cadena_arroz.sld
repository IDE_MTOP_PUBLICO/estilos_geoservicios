<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Cadena Arroz</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
         <Rule>
          <Name>1 a 227412 Ton.</Name>
            <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>227412</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#55aa00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>227413 a 454825 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>227413</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>454825</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
           <MaxScaleDenominator>25000000</MaxScaleDenominator>
           <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>454826 a 682239 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>454826</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>682239</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ff0000</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        </FeatureTypeStyle>
          </UserStyle>
        </NamedLayer>
</StyledLayerDescriptor>