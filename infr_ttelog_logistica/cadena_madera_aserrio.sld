<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Cadena Aserrio</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
         <Rule>
          <Name>1 a 31792 Ton.</Name>
            <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>31792</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#55aa00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>31793 a 63584 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>31793</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>63584</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
                 <MaxScaleDenominator>25000000</MaxScaleDenominator>
          
            <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>63585 a 95377 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>63585</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>95377</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ff0000</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        </FeatureTypeStyle>
          </UserStyle>
        </NamedLayer>
</StyledLayerDescriptor>