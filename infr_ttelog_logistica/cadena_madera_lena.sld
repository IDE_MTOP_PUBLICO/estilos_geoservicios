<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Cadena Lena</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
         <Rule>
          <Name>1 a 433854 Ton.</Name>
            <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>1</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>433854</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#55aa00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>433855 a 867709 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>433855</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>867709</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ffff00</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
               <Rule>
         <Name>867710 a 1301566 Ton.</Name>
             <ogc:Filter>
            <ogc:And>
              <ogc:PropertyIsGreaterThan>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>867710</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
              <ogc:PropertyIsLessThanOrEqualTo>
                <ogc:PropertyName>carga</ogc:PropertyName>
                <ogc:Literal>1301566</ogc:Literal>
              </ogc:PropertyIsLessThanOrEqualTo>
            </ogc:And>
          </ogc:Filter>
            <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <LineSymbolizer>
         <Stroke>
              <CssParameter name="stroke">#898989</CssParameter>
              <CssParameter name="stroke-width">3</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
         </Stroke>
            </LineSymbolizer>
          <LineSymbolizer>
                <Stroke>
              <CssParameter name="stroke">#ff0000</CssParameter>
              <CssParameter name="stroke-width">1</CssParameter>
              <CssParameter name="stroke-linecap">round</CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        </FeatureTypeStyle>
          </UserStyle>
        </NamedLayer>
</StyledLayerDescriptor>