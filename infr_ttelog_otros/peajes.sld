<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>Peaje</Name>
    <UserStyle>
      <Name>Peaje</Name>
      <Title>Peaje</Title>
      <FeatureTypeStyle>
        <Rule>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          <PointSymbolizer>
            <Graphic>
                <ExternalGraphic>
                  <OnlineResource xlink:type="simple" xlink:href="peaje_c.png" />
                  <Format>image/png</Format>
                </ExternalGraphic>
                <Size>16</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>