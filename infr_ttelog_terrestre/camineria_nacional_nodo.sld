<?xml version="1.0" encoding="UTF-8"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>Camineria Nacional</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <FeatureTypeName>Feature</FeatureTypeName>
        <Rule>
        <Name>Rutas Nacionales</Name>
          <ogc:Filter>
            <ogc:Or>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Corredor Internacional</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Ruta Primaria</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              </ogc:Or>  
          </ogc:Filter>
          <MinScaleDenominator>2000000</MinScaleDenominator>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          
          <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke" >
                  <ogc:Literal>#990000</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-opacity" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-width" >
                  <ogc:Literal>1.5</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linecap" >
                  <ogc:Literal>butt</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linejoin" >
                  <ogc:Literal>bevel</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-dashoffset" >
                  <ogc:Literal>0</ogc:Literal>
                </CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
        <Name>Rutas Nacionales</Name>
          <ogc:Filter>
            <ogc:Or>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Corredor Internacional</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Ruta Primaria</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Ruta Secundaria</ogc:Literal>
              </ogc:PropertyIsEqualTo>
              <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Ruta Terciaria</ogc:Literal>
              </ogc:PropertyIsEqualTo>
            </ogc:Or>  
          </ogc:Filter>
          <MaxScaleDenominator>2000000</MaxScaleDenominator>
          <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke" >
                  <ogc:Literal>#990000</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-opacity" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-width" >
                  <ogc:Literal>1.5</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linecap" >
                  <ogc:Literal>butt</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linejoin" >
                  <ogc:Literal>bevel</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-dashoffset" >
                  <ogc:Literal>0</ogc:Literal>
                </CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
        <Name>Camineria departamental</Name>
          <ogc:Filter>
                <ogc:PropertyIsEqualTo>
                <ogc:PropertyName>categoriza</ogc:PropertyName>
                <ogc:Literal>Caminería Departamental</ogc:Literal>
              </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <MaxScaleDenominator>1000000</MaxScaleDenominator>
          <LineSymbolizer>
              <Stroke>
                <CssParameter name="stroke" >
                  <ogc:Literal>#a4a4a4</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-opacity" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-width" >
                  <ogc:Literal>1.0</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linecap" >
                  <ogc:Literal>butt</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-linejoin" >
                  <ogc:Literal>bevel</ogc:Literal>
                </CssParameter>
                <CssParameter name="stroke-dashoffset" >
                  <ogc:Literal>0.0</ogc:Literal>
                </CssParameter>
              </Stroke>
          </LineSymbolizer>
        </Rule>
        <Rule>
           <ogc:Filter>
             <ogc:And>
              <ogc:Or>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Corredor Internacional</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Ruta Primaria</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Ruta Secundaria</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Ruta Terciaria</ogc:Literal>
                </ogc:PropertyIsEqualTo>
              </ogc:Or>  
              <ogc:And>
                <ogc:PropertyIsGreaterThan>
                  <ogc:PropertyName>numero</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
                </ogc:PropertyIsGreaterThan>
                <ogc:PropertyIsLessThanOrEqualTo>
                  <ogc:PropertyName>numero</ogc:PropertyName>
                  <ogc:Literal>500</ogc:Literal>
                </ogc:PropertyIsLessThanOrEqualTo>
              </ogc:And>
             </ogc:And>
            </ogc:Filter> 
            <MaxScaleDenominator>1500000</MaxScaleDenominator>
            <TextSymbolizer>
               <Label>
                 <ogc:PropertyName>numero</ogc:PropertyName>
               </Label>
               <Font>
                 <CssParameter name="font-family">Arial</CssParameter>
                 <CssParameter name="font-size">13</CssParameter>
                 <CssParameter name="font-weight">bold</CssParameter>
               </Font>
               <Fill>
                <CssParameter name="fill">#990000</CssParameter>
              </Fill>
              <Graphic>
                 <ExternalGraphic>
                  <OnlineResource xlink:type="simple" xlink:href="ruta.png" />
                  <Format>image/png</Format>
                </ExternalGraphic>
               </Graphic>
               <VendorOption name="graphic-resize">stretch</VendorOption>
               <VendorOption name="graphic-margin">5</VendorOption>
               <VendorOption name="spaceAround">100</VendorOption>
           </TextSymbolizer>
        </Rule>
        <Rule>
           <ogc:Filter>
             <ogc:And>
              <ogc:Or>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Corredor Internacional</ogc:Literal>
                </ogc:PropertyIsEqualTo>
                <ogc:PropertyIsEqualTo>
                  <ogc:PropertyName>categoriza</ogc:PropertyName>
                  <ogc:Literal>Ruta Primaria</ogc:Literal>
                </ogc:PropertyIsEqualTo>                
              </ogc:Or>  
              <ogc:PropertyIsGreaterThan>
                  <ogc:PropertyName>numero</ogc:PropertyName>
                  <ogc:Literal>0</ogc:Literal>
              </ogc:PropertyIsGreaterThan>
            </ogc:And>
            </ogc:Filter> 
            <MinScaleDenominator>1500000</MinScaleDenominator>
          <MaxScaleDenominator>25000000</MaxScaleDenominator>
          
            <TextSymbolizer>
               <Label>
                 <ogc:PropertyName>numero</ogc:PropertyName>
               </Label>
               <Font>
                 <CssParameter name="font-family">Arial</CssParameter>
                 <CssParameter name="font-size">9</CssParameter>
                 <CssParameter name="font-weight">bold</CssParameter>
               </Font>
               <Fill>
                <CssParameter name="fill">#990000</CssParameter>
              </Fill>
              <Graphic>
                 <ExternalGraphic>
                  <OnlineResource xlink:type="simple" xlink:href="ruta.png" />
                  <Format>image/png</Format>
                </ExternalGraphic>
               </Graphic>
               <VendorOption name="graphic-resize">stretch</VendorOption>
               <VendorOption name="graphic-margin">4</VendorOption>
               <VendorOption name="spaceAround">35</VendorOption>
           </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>