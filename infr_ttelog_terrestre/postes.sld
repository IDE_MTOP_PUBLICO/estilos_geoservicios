<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>Postes kilometricos</Name>
    <UserStyle>
      <Name>Postes kilometricos</Name>
      <Title>Postes kilometricos</Title>
      <Abstract>boton con imagen de Postes kilometricos</Abstract>

      <FeatureTypeStyle>
        <Rule>
        <Title>Postes kilometricos</Title>
          <!--Se filtra según la atura para mostrar los postes-->
          <ogc:Filter>
            
            <ogc:And>
              <!--Chequeo que el campo km no esté vacío para evitar errores-->
              <ogc:Not>
                <ogc:PropertyIsNull>
                  <ogc:PropertyName>km</ogc:PropertyName>
                </ogc:PropertyIsNull>
              </ogc:Not>
            
              <!--Calculo el resto del valor km entre 1, 5, 10 y 20 y lo comparo con 0-->
              <ogc:PropertyIsEqualTo>  
                <ogc:Function name="IEEERemainder"> <!--Funcion que calcula el resto-->   
                  <ogc:PropertyName>km</ogc:PropertyName>
                    
                    <ogc:Function name="if_then_else">
                      
                      <!--Si la altura es mayor a 1000000-->
                      <ogc:Function name="GreaterThan">
                        <ogc:Function name="parseDouble"> <!--los campos a comparar se castean a tipo doble-->
                          <ogc:Function name="env">
                           <ogc:Literal>wms_scale_denominator</ogc:Literal> <!--funcion que calcula la altura-->
                          </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parseDouble">
                          <ogc:Literal>1000000.0</ogc:Literal>
                        </ogc:Function>  
                      </ogc:Function> 
                      
                      <!--Entonces, calculo el resto entre 20 (para mostrar los postes cada 20 km)-->
                      <ogc:Literal>20</ogc:Literal>
                      
                      <!--Sino, si la altura es mayor a 500000-->
                      <ogc:Function name="if_then_else">
                        <ogc:Function name="GreaterThan">
                          <ogc:Function name="parseDouble">
                            <ogc:Function name="env">
                             <ogc:Literal>wms_scale_denominator</ogc:Literal>
                            </ogc:Function>
                          </ogc:Function>
                          <ogc:Function name="parseDouble">
                            <ogc:Literal>500000.0</ogc:Literal>
                          </ogc:Function>  
                        </ogc:Function> 
                      
                        <!--Entonces, calculo el resto entre 10 (para mostrar los postes cada 10 km)-->
                        <ogc:Literal>10</ogc:Literal>
                        
                        <!--Sino, si la altura es mayor a 140000-->
                        <ogc:Function name="if_then_else">
                          <ogc:Function name="GreaterThan">
                            <ogc:Function name="parseDouble">
                              <ogc:Function name="env">
                                <ogc:Literal>wms_scale_denominator</ogc:Literal>
                              </ogc:Function>
                            </ogc:Function>
                            <ogc:Function name="parseDouble">
                              <ogc:Literal>140000.0</ogc:Literal>
                            </ogc:Function>  
                          </ogc:Function> 
                        
                          <!--Entonces, calculo el resto entre 5 (para mostrar los postes cada 5 km)-->
                          <ogc:Literal>5</ogc:Literal>
                        
                          <!--Sino, calculo el resto entre 1 (para mostrar todos los postes)-->
                          <ogc:Literal>1</ogc:Literal>
                        
                        </ogc:Function> <!--if_then_else-->
                      </ogc:Function> <!--if_then_else-->
                    </ogc:Function> <!--if_then_else-->
                  
                  </ogc:Function><!--IEEERemainder-->
                
                  <!--El resto se compara con cero-->
                  <ogc:Literal>0</ogc:Literal>
                
              </ogc:PropertyIsEqualTo>
            </ogc:And>
          </ogc:Filter>
          
          <!--Los postes se muestran a partir de esta altura-->
          <MaxScaleDenominator>1800000</MaxScaleDenominator>
     
          <PointSymbolizer>
            <Graphic>
              <ExternalGraphic>
                <OnlineResource xlink:type="simple" xlink:href="posteverde.png" />
                <Format>image/png</Format>
              </ExternalGraphic>
              <Size>20</Size>
            </Graphic>
          </PointSymbolizer>
          
          <!--Esta etiqueta se usó como forma de visualizar el valor que devuelve la función, 
              se deja comentada para futuras referencias-->
          <!--TextSymbolizer>
            <Label>
               <ogc:Function name="env">
               <ogc:Literal>wms_scale_denominator</ogc:Literal>
               </ogc:Function>
            </Label>
          </TextSymbolizer-->
              
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>