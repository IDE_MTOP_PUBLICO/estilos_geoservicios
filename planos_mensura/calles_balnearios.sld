<?xml version="1.0" encoding="ISO-8859-1"?>

<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>calles_balnearios</Name>
    <UserStyle>
      <Title>calles de balnearios</Title>
      
      <FeatureTypeStyle>
     <Rule>
       <Name>calles</Name>
          <LineSymbolizer>
         <!--Stroke>
           <CssParameter name="stroke">#FF0000</CssParameter>
         </Stroke-->
       </LineSymbolizer>
       <MaxScaleDenominator>20000</MaxScaleDenominator>
       <TextSymbolizer>
         <Label>
           <ogc:PropertyName>nombre</ogc:PropertyName>
         </Label>
         <LabelPlacement>
           <LinePlacement />
         </LabelPlacement>
         <Fill>
           <CssParameter name="fill">#000000</CssParameter>
         </Fill>
         <VendorOption name="followLine">true</VendorOption>
       </TextSymbolizer>
     </Rule>
   </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>