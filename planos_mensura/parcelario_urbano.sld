<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" 
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
 xmlns="http://www.opengis.net/sld" 
 xmlns:ogc="http://www.opengis.net/ogc" 
 xmlns:xlink="http://www.w3.org/1999/xlink" 
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <!-- a Named Layer is the basic building block of an SLD document -->
  <NamedLayer>
    <Name>Parcelario</Name>
    <UserStyle>
      <Title>Parcelario Catastro - CDP</Title>
        <FeatureTypeStyle>
          <Rule>
            <Name>Poligono cercano</Name>
            <Title>Parcelario urbano</Title>
            <MaxScaleDenominator>5000</MaxScaleDenominator>
            <PolygonSymbolizer>
              <Fill>
                <CssParameter name="fill">#E6E6E6</CssParameter>
                <CssParameter name="fill-opacity">0.8</CssParameter>
              </Fill>
              <Stroke>
                <CssParameter name="stroke">#000000</CssParameter>
                <CssParameter name="stroke-width">1</CssParameter>
                <CssParameter name="stroke-opacity">0.5</CssParameter>
              </Stroke>
            </PolygonSymbolizer>
            <TextSymbolizer>
              <Label>
                  <ogc:PropertyName>numeropadr</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">SansSerif</CssParameter>
                  <CssParameter name="font-weight">bold</CssParameter>
                  <CssParameter name="font-size">12</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.5</AnchorPointY>
                   </AnchorPoint>
                 </PointPlacement>
              </LabelPlacement>
              <Halo>
                   <Radius>2</Radius>
                   <Fill>
                         <CssParameter name="fill">#E6E6E6</CssParameter>
                   </Fill>
              </Halo>
              <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
              </Fill>
          </TextSymbolizer>
          </Rule>
          <Rule>
            <Name>Poligono lejano</Name>
            <Title>Parcelario urbano</Title>
            <MinScaleDenominator>5000</MinScaleDenominator>
            <MaxScaleDenominator>120000</MaxScaleDenominator>
            <PolygonSymbolizer>
              <Stroke>
                <CssParameter name="stroke">#000000</CssParameter>
                <CssParameter name="stroke-width">1</CssParameter>
                <CssParameter name="stroke-opacity">0.5</CssParameter>
              </Stroke>
            </PolygonSymbolizer>
          </Rule>
                    
          <Rule>
            <MinScaleDenominator>5000</MinScaleDenominator>
            <MaxScaleDenominator>20000</MaxScaleDenominator>
            <TextSymbolizer>
              <Label>
                  <ogc:PropertyName>numeropadr</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">SansSerif</CssParameter>
                  <CssParameter name="font-weight">bold</CssParameter>
                  <CssParameter name="font-size">9</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.5</AnchorPointY>
                   </AnchorPoint>
                 </PointPlacement>
              </LabelPlacement>
              <Halo>
                   <Radius>2</Radius>
                   <Fill>
                         <CssParameter name="fill">#E0DAD2</CssParameter>
                   </Fill>
              </Halo>
              <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
              </Fill>
              
          </TextSymbolizer>
        </Rule>         
        
        
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>