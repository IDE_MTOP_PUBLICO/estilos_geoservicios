<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.opengis.net/sld" xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" version="1.0.0" >
  <NamedLayer>
    <Name>planos_ptos</Name>
    <UserStyle>
      <Title>Plano sin georeferenciar</Title>
      <FeatureTypeStyle>
        <Rule>
          <Name>planos_ptos</Name>
          <!--MinScaleDenominator>100000</MinScaleDenominator>
          <MaxScaleDenominator>200000</MaxScaleDenominator-->
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#B18904</CssParameter>
                </Fill>
                <Stroke>
                  <CssParameter name="stroke">#000000</CssParameter>
                </Stroke>
              </Mark>
              <Size>11</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Name>planos_ptos_text</Name>
          <MaxScaleDenominator>7000</MaxScaleDenominator>
            <TextSymbolizer>
              <Label>
                  <ogc:PropertyName>autogen</ogc:PropertyName>
              </Label>
              <Font>
                  <CssParameter name="font-family">SansSerif</CssParameter>
                  <CssParameter name="font-weight">bold</CssParameter>
                  <CssParameter name="font-size">11</CssParameter>
              </Font>
              <LabelPlacement>
                 <PointPlacement>
                   <AnchorPoint>
                     <AnchorPointX>0.5</AnchorPointX>
                     <AnchorPointY>0.0</AnchorPointY>
                   </AnchorPoint>
                   <Displacement>
                     <DisplacementX>0</DisplacementX>
                     <DisplacementY>5</DisplacementY>
                   </Displacement>
                 </PointPlacement>
              </LabelPlacement>
              <Halo>
                   <Radius>2</Radius>
                   <Fill>
                         <CssParameter name="fill">#D8D8D8</CssParameter>
                   </Fill>
              </Halo>
              <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
              </Fill>
          </TextSymbolizer>
        </Rule>
       
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>